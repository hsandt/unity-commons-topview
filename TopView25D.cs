﻿using UnityEngine;
using System.Collections;

using CommonsPattern;

/// Singleton class containing parameters and functions for 2.5D operations
/// The imaginary world is seen from an inclined bird view, but in reality the camera looks at the game plan
/// and the sprites orthogonally. The 2.5D world consists of a theoretically continuous succession Z-layers,
/// where Z represents the imaginary altitude. If plan B has a Z superior to plan A by deltaZ, then it is also
/// offseted by sin(view angle) * deltaZ in the Y positive direction (the 2D "up").
/// Use Vector3.forward for the positive vertical direction, but it has no meaning regarding Unity coordinates.
public class TopView25D : SingletonManager<TopView25D> {

	protected TopView25D () {} // guarantee this will be always a singleton only - can't use the constructor!

	void Awake () {
		SetInstanceOrSelfDestruct(this);
	}

	// parameters
	/// View angle of the camera from the imaginary vertical up direction,
	/// if the world existed in 3D (30° close to top view, 45° for 3/4, 60° close to side view)
	[SerializeField]
	float viewAngle = 0f;

	/// Return 3D unit vector corresponding to the ground to sky direction as suggested by the appearance of the sprites
	public static Vector3 GetVector25DUp () {
		return Vector3.zero;
	}

	/// Return the position of the projection to ground corresponding to 2.5D position (shadow position for a vertical light source)
	public static Vector3 GetGroundProjection (Vector3 position) {
		return Vector3.zero;
	}

	/// Return the Y offset of the view corresponding to the altitude z, for a given view angle
	public static float GetYOffset(float z, float viewAngle) {
		return Mathf.Sin(viewAngle * Mathf.Deg2Rad) * z;
	}

	/// Return the Y offset of the view corresponding to the altitude z, for the set view angle
	public float GetYOffset(float z) {
		return GetYOffset(z, Instance.viewAngle);
	}

}

﻿using UnityEngine;
using System.Collections;

using CommonsPattern;

public class Physics25D : SingletonManager<Physics25D> {

	protected Physics25D () {} // guarantee this will be always a singleton only - can't use the constructor!

	void Awake () {
		SetInstanceOrSelfDestruct(this);
	}

	/* Parameters */
	[SerializeField]
	float m_GravityScale = 9.81f;
	/// Gravity scale
	public static float gravityScale { get { return Instance.m_GravityScale; } }

}

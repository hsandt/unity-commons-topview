﻿using UnityEngine;
using System.Collections;

using CommonsDebug;
using CommonsHelper;
using UnityConstants;

/// Motion 2.5D simulates 3D movement in a 2D space by adding a Z coordinate
[RequireComponent (typeof (Rigidbody2D))]
public class Motion25D : MonoBehaviour {

	/* Sibling components */
	Rigidbody2D rigidbody2d;  // contains 2D position and velocity on XY

	/* Children components */
	/// Transform of the sprite that represents this object based on its altitude
	[SerializeField]
	Transform spriteTr = null;

	/* Parameters */
	/// Base name of the layers on which this object is moving (without Z0, Z1, etc.)
	[SerializeField]
	string layerBaseName = "";

	/* Derived data */
	int[] zLayers = new int[2];

	/* State vars */
	/// Fictive altitude (Z coordinate)
	float z;
	/// Fictive vertical velocity (Z derivative)
	float velocityZ;
	/// Altitude layer index (0 for ground, then 1, 2, ... toward the top)
	int m_LayerZ;
	public int layerZ { get { return m_LayerZ; } set { m_LayerZ = value; gameObject.layer = zLayers[value]; } }

	/* Properties */
	/// 3D velocity as if the game was seen from a perfect top-down view:
	/// Z stands for exact altitude
	public Vector3 velocity {
		get { return new Vector3(rigidbody2d.velocity.x, rigidbody2d.velocity.y, velocityZ); }
		set { rigidbody2d.velocity = new Vector2(value.x, value.y); velocityZ = value.z; }
	}

	// Use this for initialization
	void Awake () {
		rigidbody2d = this.GetComponentOrFail<Rigidbody2D>();

		// store layers from base name + Zi
		for (int i = 0; i < 2; ++i) {
			zLayers[i] = LayerMask.NameToLayer(string.Format("{0}Z{1}", layerBaseName, i));
		}
	}

	void Start () {
		Setup();
	}

	public void Setup () {
		// IMPROVE: objects that start on the upper level should get the correspond z / layerZ
		z = 0;
		velocityZ = 0;
		layerZ = 0;
		UpdateLayer();

		DebugScreen.PrintVar<float>("sprite Y", 0);
	}

	void FixedUpdate () {

		// apply gravity acceleration
		velocityZ -= Physics25D.gravityScale * Time.deltaTime;

		// apply Z velocity
		z += velocityZ * Time.deltaTime;

		// stop motion on ground (Z = 0) immediately (sticky ground, including in XY directions)
		if (z < 0) {
			z = 0;
			// velocityZ = 0;
			velocity = Vector3.zero;
		}

		UpdateLayer();
		UpdateSortingLayer();

		// show sprite at the correct Y, based on the actual Y and Z
		// spriteTr.localPosition = new Vector3(0, TopView25D.Instance.GetYOffset(z), 0);
		// DebugScreen.PrintVar<float>("sprite Y", spriteTr.localPosition.y);
	}

	void Update () {
		// show sprite at the correct Y, based on the actual Y and Z
		spriteTr.localPosition = new Vector3(0, TopView25D.Instance.GetYOffset(z), 0);
		DebugScreen.PrintVar<float>("sprite Y", spriteTr.localPosition.y);
	}

	void OnEnable() {
	}

	void OnDisable() {
	}

	void UpdateLayer () {
		// objects under z=1 should be on ground layer Z0, others on upper layer Z1
		if (layerZ == 0 && z >= 1f) {
			Debug.LogFormat("{0} -> Z1", this);
			layerZ = 1;
		} else if (layerZ == 1 && z < 1f) {
			Debug.LogFormat("{0} -> Z0", this);
			layerZ = 0;
		}
	}

	void UpdateSortingLayer () {

	}

}

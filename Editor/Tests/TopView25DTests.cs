﻿using UnityEngine;
using System.Collections;

using NUnit.Framework;

[TestFixture]
public class TopView25DTests {

	[Test]
	public void GetYOffset_Z0ViewAngle0_Offset0 () {
		float yOffset = TopView25D.GetYOffset(0f, 0f);
		Assert.AreEqual(0f, yOffset);
	}

	[Test]
	public void GetYOffset_Z1ViewAngle0_Offset0 () {
		float yOffset = TopView25D.GetYOffset(1f, 0f);
		Assert.AreEqual(0f, yOffset);
	}

	[Test]
	public void GetYOffset_Z1ViewAngle30_OffsetLow () {
		float yOffset = TopView25D.GetYOffset(1f, 30f);
		Assert.AreEqual(0.5f, yOffset);
	}

	[Test]
	public void GetYOffset_Z0ViewAngle45_Offset0 () {
		float yOffset = TopView25D.GetYOffset(0f, 45f);
		Assert.AreEqual(0f, yOffset);
	}

	[Test]
	public void GetYOffset_Z1ViewAngle45_OffsetMedium () {
		float yOffset = TopView25D.GetYOffset(1f, 45f);
		Assert.AreEqual(Mathf.Sqrt(2) / 2, yOffset, 1E-4);
	}

	[Test]
	public void GetYOffset_Z1ViewAngle60_OffsetHigh () {
		float yOffset = TopView25D.GetYOffset(1f, 60f);
		Assert.AreEqual(Mathf.Sqrt(3) / 2, yOffset, 1E-4);
	}

	[Test]
	public void GetYOffset_Z0ViewAngle90_Offset0 () {
		float yOffset = TopView25D.GetYOffset(0f, 90f);
		Assert.AreEqual(0f, yOffset);
	}

	[Test]
	public void GetYOffset_Z1ViewAngle90_Offset1 () {
		float yOffset = TopView25D.GetYOffset(1f, 90f);
		Assert.AreEqual(1f, yOffset);
	}

}

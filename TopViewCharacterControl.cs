﻿using UnityEngine;
using System.Collections;

public class TopViewCharacterControl : MonoBehaviour {

	/// Vector characterizing move intention, magnitude between 0 and 1
	protected Vector3 m_MoveIntentionVector;
	public Vector3 moveIntentionVector {
		get { return m_MoveIntentionVector; }
		set {
			// set vector, clipping to unit magnitude
			m_MoveIntentionVector = value.magnitude <= 1 ? value : value.normalized;
		}
	}

}

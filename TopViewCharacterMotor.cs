﻿using UnityEngine;
using System.Collections;

using CommonsHelper;

// RENAME: pure top view uses Rigidbody2D; this script is actually ThreeQuarterView
[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof (TopViewCharacterControl))]
[RequireComponent(typeof (Animator))]
public class TopViewCharacterMotor : MonoBehaviour {

	/* sibling components */
	Rigidbody m_Rigidbody;
	TopViewCharacterControl control;
	Animator animator;

	/* parameters */
	[SerializeField]
	float speed = 1f;

	void Awake () {
		m_Rigidbody = this.GetComponentOrFail<Rigidbody>();
		control = this.GetComponentOrFail<TopViewCharacterControl>();
		animator = this.GetComponentOrFail<Animator>();
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void FixedUpdate () {
		Move(control.moveIntentionVector);
	}

	/// Apply move, a vector in the world coordinates with magnitude equal to actual speed wanted
	void Move(Vector3 moveVector) {
		Vector3 moveVelocity = moveVector * speed;

		// update Rigidbody velocity
		m_Rigidbody.velocity = moveVelocity;

		// synchronize values with animator (or use a Property)
		// because FSM transition conditions are limited in Unity, there is some redundancy
		// IMPROVE: if character walks on a slope, needs to adapt move velocity
		animator.SetFloat("VelocityX", moveVelocity.x);
		animator.SetFloat("VelocityY", moveVelocity.z);  // top view uses XZ plan for motion
		animator.SetBool("IsWalking", moveVelocity != Vector3.zero);
	}

	// REFACTOR: put that in TopView or Vector conversion utility
	/// Convert a 2D ground vector to a world vector, if XZ is the ground plane
	// public static Vector3 ToWorldVector(Vector2 groundVector) {
		// return new Vector3(groundVector.x, 0, groundVector.y);
	// }

}

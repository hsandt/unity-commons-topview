using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class TopViewPlayerCharacterControl : TopViewCharacterControl {

	/* external references */
	Transform mainCamera;

	/* parameters */
	// should we use camera-relative controls if there is a main camera?
	[SerializeField]
	bool m_UseCameraRelativeControls = false;

	// Use this for initialization
	void Awake () {
		if (Camera.main == null) {
			Debug.LogWarning("Warning: no main camera found. Cannot use camera-relative controls.");
		}
		else {
			mainCamera = Camera.main.transform;
		}
	}

	// Update is called once per frame
	void Update () {
		float h = CrossPlatformInputManager.GetAxis("Horizontal");
		float v = CrossPlatformInputManager.GetAxis("Vertical");

		if (m_UseCameraRelativeControls && mainCamera != null) {
			// calculate camera relative direction to move (if camera roll is null, motion on XZ plane)
			Vector3 cameraPlaneForward = Vector3.Scale(mainCamera.forward, new Vector3(1, 0, 1)).normalized;
			moveIntentionVector = v * cameraPlaneForward + h * mainCamera.right;
			Debug.LogFormat("cameraPlaneForward: {0}", cameraPlaneForward);
		} else {
			// world-relative control
			// (we use Vector3.left because our camera is placed facing X in the negative direction)
			moveIntentionVector = v * Vector3.forward + h * Vector3.right;
		}
	}

}
